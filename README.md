# Relaxing Game Project
## PlayerControl scene
### Shaders
- Rain flowing on the wall
- Rain Ripple on the floor
- Refraction and reflection on the aquarium glass
- Rim light on the character
### Input System
- Control character with finite state machine
## VideoPlayer scene
### UI
- Control dancing animation and timeline by each other
