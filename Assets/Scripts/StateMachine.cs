﻿using System;
using System.Collections.Generic;

public interface IState
{
    void OnEnter();
    void OnLoop();
    void OnExit();
}

public class StateMachine
{
   private IState currentState;
   
   private Dictionary<Type, List<Transition>> transitionDictionary = new Dictionary<Type,List<Transition>>();
   private List<Transition> currentTransition = new List<Transition>();
   private List<Transition> anyTransition = new List<Transition>();
   
   private static List<Transition> emptyTransition = new List<Transition>(0);

   public void Check()
   {
      var transition = GetTransition();
      if (transition != null)
         SetState(transition.Next);
      
      currentState?.OnLoop();
   }

   public void SetState(IState state)
   {
      if (state == currentState)
         return;
      
      currentState?.OnExit();
      currentState = state;
      
      transitionDictionary.TryGetValue(currentState.GetType(), out currentTransition);
      if (currentTransition == null)
         currentTransition = emptyTransition;
      
      currentState.OnEnter();
   }

   public void AddTransition(IState current, IState next, Func<bool> condition)
   {
      if (transitionDictionary.TryGetValue(current.GetType(), out var transition) == false)
      {
         transition = new List<Transition>();
         transitionDictionary[current.GetType()] = transition;
      }
      
      transition.Add(new Transition(next, condition));
   }

   public void AnyCondition(IState next, Func<bool> condition)
   {
      anyTransition.Add(new Transition(next, condition));
   }

   private class Transition
   {
      public Func<bool> Condition {get; }
      public IState Next { get; }

      public Transition(IState next, Func<bool> condition)
      {
         Next = next;
         Condition = condition;
      }
   }

   private Transition GetTransition()
   {
      foreach(var transition in anyTransition)
         if (transition.Condition())
            return transition;
      
      foreach (var transition in currentTransition)
         if (transition.Condition())
            return transition;

      return null;
   }
}