﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatorSlider : MonoBehaviour
{
    public Animator animator;
    public Slider animatorSlider;
    private void Update()
    {
        animatorSlider.value = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    public void OnValueChanged()
    {
        //animatorSlider.onValueChanged.AddListener(value => animator.Play(animator.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, value));//要搭配RemoveAllListeners
        animator.Play(animator.GetCurrentAnimatorStateInfo(0).fullPathHash, 0, animatorSlider.normalizedValue);
    }
}
