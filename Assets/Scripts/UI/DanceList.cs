﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DanceList : MonoBehaviour
{
    public Dropdown danceList;
    public Slider animatorSlider;

    public Animator animator;
    private void Awake()
    {
        danceList.onValueChanged.AddListener(delegate { OnValueChanged(danceList); });
    }
    public void OnValueChanged(Dropdown danceList) 
    {
        switch (danceList.value)
        {
            case 1:
                animator.SetTrigger("Dance1");
                break;
            case 2:
                animator.SetTrigger("Dance2");
                break;
            case 3:
                animator.SetTrigger("Dance3");
                break;
            case 4:
                animator.SetTrigger("Dance4");
                break;
        }
        danceList.value = 0;
    }
}
