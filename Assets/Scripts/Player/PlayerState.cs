using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerState : MonoBehaviour
{
    private Animator animator;
    private StateMachine stateMachine;
    [HideInInspector] public float forward;
    [HideInInspector] public float turn;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        stateMachine = new StateMachine();
        PlayerIdle playerIdle = new PlayerIdle(this, animator);
        PlayerLocomotion playerLocomotion = new PlayerLocomotion(this, animator);

        void Condition(IState current, IState next, Func<bool> condition) => stateMachine.AddTransition(current, next, condition);
        Condition(playerIdle, playerLocomotion, () => forward != 0 || turn != 0);
        Condition(playerLocomotion, playerIdle, () => forward == 0 && turn == 0);

        stateMachine.SetState(playerIdle);
    }
    void Update()
    {
        stateMachine.Check();
    }
    public void OnForward(InputAction.CallbackContext context)
    {
        forward = context.ReadValue<float>();
    }
    public void OnTurn(InputAction.CallbackContext context)
    {
        turn = context.ReadValue<float>();
    }
}
