using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocomotion : IState
{
    private PlayerState player;
    private Animator animator;
    private int animatorForwardHash;
    private int animatorTurnHash;

    public PlayerLocomotion(PlayerState player, Animator animator)
    {
        this.player = player;
        this.animator = animator;
        animatorForwardHash = Animator.StringToHash("Forward");
        animatorTurnHash = Animator.StringToHash("Turn");
    }
    public void OnEnter() { }
    public void OnLoop()
    {
        animator.SetFloat(animatorForwardHash, Mathf.MoveTowards(animator.GetFloat(animatorForwardHash), player.forward, 0.1f));
        animator.SetFloat(animatorTurnHash, Mathf.MoveTowards(animator.GetFloat(animatorTurnHash), player.turn, 0.1f));
    }
    public void OnExit() { }
}
